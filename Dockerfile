FROM python:3.10-buster
MAINTAINER Flowelfox <flowelfox@gmail.com>

# Copying sources to image and installing bot dependencies
RUN mkdir /bot
COPY . /bot
WORKDIR /bot/
# Installing botmanlib and clearing dependencies
RUN apt-get update
RUN apt-get install -y python-pil
RUN pip install poetry
RUN pip install -e .
RUN poetry install

RUN mv /bot/resources /bot/bot_resources

# Creating command for starting container
CMD cp -r /bot/bot_resources/* /bot/resources;python src/app.py
