import ast

import os
from distutils.util import strtobool
from os import path
from pathlib import Path
from dotenv import load_dotenv

from src.logs import setup_logging

load_dotenv()
"""-----PATHS-----"""
PROJECT_ROOT = Path(path.normpath(path.join(path.dirname(path.realpath(__file__)), '..')))
RESOURCES_FOLDER = PROJECT_ROOT / 'resources'
SETTINGS_FILE = RESOURCES_FOLDER / 'settings.json'
MIGRATIONS_PATH = PROJECT_ROOT / 'spacegame' / 'database' / 'migrations'
"""-----JOBS SETTINGS-----"""
MAIN_JOBS_PATH = RESOURCES_FOLDER / 'main_jobs.sqlite'
"""-----BOT SETTINGS-----"""
TIMEZONE = "Europe/Kiev"
IMAGE_WAIT_TIME = 10
"""-----WEBHOOK SETTINGS-----"""
WEBHOOK_ENABLE = False
WEBHOOK_URL = "/"
WEBHOOK_IP = '127.0.0.1'
WEBHOOK_PORT = -1
"""-----EMAIL SETTINGS-----"""
SENDER_SERVER = ""
SENDER_SERVER_PORT = None
SENDER_USERNAME = ""
SENDER_PASSWORD = ""
SENDER_EMAIL = ""
"""-----DATABASE SETTINGS-----"""
DATABASE_URL = ""
TESTING_DATABASE_URL = ""  # Without database name. Example: postgresql://login:password@localhost
POOL_SIZE = 5
MAX_OVERFLOW = 10
"""-----BOT SETTINGS-----"""
BOT_TOKEN = ""
BOT_USERNAME = ""
SUPERUSER_ACCOUNTS = [262902863]
SUPPORT_ACCOUNT = 262902863

# Parse environment variables
vars_copy = locals().copy()
local_variables = locals()
for key in vars_copy:
    if key in os.environ:
        if os.environ[key] == "None":
            local_variables[key] = None
            continue

        if os.environ[key].startswith('"') and os.environ[key].endswith('"'):
            local_variables[key] = os.environ[key].replace('"', "")
            continue

        if os.environ[key].startswith('[') and os.environ[key].endswith(']'):
            local_variables[key] = ast.literal_eval(os.environ[key])
            continue

        if '.' in os.environ[key]:
            try:
                local_variables[key] = float(os.environ[key])
                continue
            except ValueError:
                pass

        try:
            local_variables[key] = int(os.environ[key])
            continue
        except ValueError:
            pass

        try:
            local_variables[key] = bool(strtobool(os.environ[key]))
            continue
        except ValueError:
            pass

            local_variables[key] = os.environ[key]

setup_logging(PROJECT_ROOT)
