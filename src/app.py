import datetime
import logging
import os
import pprint
import traceback
from gettext import gettext

from telegram import Message, Chat
from telegram.error import TelegramError
from telegram.ext import ApplicationBuilder, PrefixHandler, CallbackQueryHandler, MessageHandler, filters, \
    ContextTypes

from src.database.models import User
from src.jobs import start_check_blocked_job, stop_check_blocked_job, start_send_post_job, stop_send_post_job
from src.lib.callback_context import CustomCallbackContext
from src.menus.fallback import goto_start
from src.menus.like_dislike import post_like, post_dislike
from src.menus.start import StartMenu
from src.settings import RESOURCES_FOLDER, SETTINGS_FILE, WEBHOOK_ENABLE, WEBHOOK_IP, WEBHOOK_URL, WEBHOOK_PORT, \
    BOT_TOKEN

logger = logging.getLogger(__name__)


async def stop(update, context):
    DBSession = context.session
    # if user never have a conversation with bot and sending /stop he has empty user_data
    if context.user_data:
        _ = context.user_data['_'] if '_' in context.user_data else gettext
        user = DBSession.query(User).filter(User.id == context.user_data['user'].id).first()
        user.block_date = datetime.datetime.utcnow()
        user.chat_room_id = None
        DBSession.add(user)
        DBSession.commit()
        await context.bot.send_message(chat_id=user.id, text=_("You are deleted from this bot."))


async def error(update, context):
    """Log Errors caused by Updates."""
    if update:
        pp = pprint.PrettyPrinter(indent=4)
        logger.error(f'Update "{pp.pformat(update.to_dict())}" caused error \n{context.error}"')
    traceback.print_exc()


def main():
    context_types = ContextTypes(context=CustomCallbackContext)
    application = ApplicationBuilder().token(BOT_TOKEN).context_types(context_types).build()

    start_menu = StartMenu(application=application)
    stop_handler = PrefixHandler('/', 'stop', stop)

    # adding menus
    application.add_handler(stop_handler)
    application.add_handler(CallbackQueryHandler(post_like, pattern=r"^like_post_\d+$"))
    application.add_handler(CallbackQueryHandler(post_dislike, pattern=r"^dislike_post_\d+$"))
    application.add_handler(CallbackQueryHandler(lambda update, context: update.effective_message.delete(), pattern="^close_message$"))
    application.add_handler(start_menu.handler)
    application.add_handler(MessageHandler(filters.ALL, goto_start))
    application.add_handler(CallbackQueryHandler(goto_start))
    application.add_error_handler(error)

    # Start bot
    if not os.path.exists(RESOURCES_FOLDER):
        os.mkdir(RESOURCES_FOLDER)
        logger.info("Resources folder created")
    if not os.path.exists(SETTINGS_FILE):
        with open(str(SETTINGS_FILE) + ".default", 'r') as settings:
            out = open(str(SETTINGS_FILE), 'w')
            out.write(settings.read())
            out.close()
        logger.info("Settings file created")

    start_check_blocked_job(application.job_queue)
    start_send_post_job(application.job_queue)

    logger.info("Bot started")
    if WEBHOOK_ENABLE:
        application.run_webhook(listen=WEBHOOK_IP, port=WEBHOOK_PORT, webhook_url=WEBHOOK_URL,
                                allowed_updates=['message', 'edited_message', 'callback_query'])
    else:
        application.run_polling(allowed_updates=['message', 'edited_message', 'callback_query'])


    stop_check_blocked_job(application.job_queue)
    stop_send_post_job(application.job_queue)


if __name__ == "__main__":
    main()
