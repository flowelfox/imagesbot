import datetime
import logging

import pytz
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import Forbidden, BadRequest

from src.lib.helpers import get_settings
from src.database.models import Post, Image
from src.database.models import User
from src.settings import TIMEZONE, SETTINGS_FILE, SUPERUSER_ACCOUNTS

logger = logging.getLogger(__name__)


def start_check_blocked_job(job_queue):
    stop_check_blocked_job(job_queue)
    job_queue.run_repeating(check_blocked_job, 60 * 120, first=0, name="check_blocked_job")
    logging.info("Check blocked users job started...")


def stop_check_blocked_job(job_queue):
    for job in job_queue.get_jobs_by_name("check_blocked_job"):
        job.schedule_removal()
        logging.info("Check blocked users job stopped...")


async def check_blocked_job(context):
    BlockSession = context.session

    users = BlockSession.query(User).all()
    for user in users:
        try:
            context.bot.send_chat_action(user.chat_id, 'typing')
        except (Forbidden, BadRequest):
            user.deactivate()
            BlockSession.add(user)

    BlockSession.commit()


def start_send_post_job(job_queue):
    stop_send_post_job(job_queue)
    job_queue.run_repeating(send_post_job, 5, first=0, name="send_post_job", data={"admin_notification_sent": False})
    logging.info("Send post job started...")


def stop_send_post_job(job_queue):
    for job in job_queue.get_jobs_by_name("send_post_job"):
        job.schedule_removal()
        logging.info("Send post job stopped...")


async def send_post_job(context):
    JobSession = context.session

    utcnow = datetime.datetime.utcnow()
    current_utc_time = utcnow.time()
    current_utc_hour = current_utc_time.hour
    current_utc_minute = current_utc_time.minute

    now = pytz.utc.localize(utcnow).astimezone(pytz.timezone(TIMEZONE)).replace(tzinfo=None)
    current_time = now.time()
    current_hour = current_time.hour
    current_minute = current_time.minute

    settings = await get_settings(SETTINGS_FILE)
    posting_time = settings['posting_time']

    posting_time_converted = []
    for pt in posting_time:
        h, m = pt.split(":")
        posting_time_converted.append((int(h), int(m)))

    if (current_hour, current_minute) in posting_time_converted:
        # check if we already sent post at this time
        post_max_time = utcnow.replace(hour=current_utc_hour, minute=current_utc_minute+1, second=0, microsecond=0)
        post_min_time = utcnow.replace(hour=current_utc_hour, minute=current_utc_minute, second=0, microsecond=0)
        sent_post = JobSession.query(Post).filter(Post.create_date < post_max_time).filter(Post.create_date >= post_min_time).first()
        if sent_post is not None:
            return

        admins = JobSession.query(User).filter(User.id.in_(SUPERUSER_ACCOUNTS)).all()
        images_left = JobSession.query(Image).filter(Image.post == None).count()
        not_used_image = JobSession.query(Image).filter(Image.post == None).order_by(Image.create_date.asc()).first()
        if not_used_image is None:
            for admin in admins:
                _ = admin.translator
                admin_buttons = [[InlineKeyboardButton(_("Close message"), callback_data="close_message")]]
                await context.bot.send_message(chat_id=admin.id, text=_("⚠ Warning ⚠\n\nThere is no unused images left. Please upload more images"), reply_markup=InlineKeyboardMarkup(admin_buttons))
        elif images_left < 5:
            for admin in admins:
                _ = admin.translator
                admin_buttons = [[InlineKeyboardButton(_("Close message"), callback_data="close_message")]]
                await context.bot.send_message(chat_id=admin.id, text=_("⚠ Warning ⚠\n\nThere are only {images_left} images left. Please upload more images").format(images_left=images_left), reply_markup=InlineKeyboardMarkup(admin_buttons))

        if not_used_image is not None:
            # generate and send post to channel
            post = Post(image=not_used_image)
            JobSession.add(post)
            JobSession.commit()

            buttons = [[InlineKeyboardButton("♥ 0", callback_data=f"like_post_{post.id}"),
                        InlineKeyboardButton("💔 0", callback_data=f"dislike_post_{post.id}")]]

            await context.bot.send_photo(chat_id=settings['channel']['id'], photo=not_used_image.file_id, reply_markup=InlineKeyboardMarkup(buttons))


