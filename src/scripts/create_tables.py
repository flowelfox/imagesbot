from sqlalchemy import create_engine, text
from sqlalchemy.orm import Session

from src.database.models import *  # noqa: F401, F403
from src.database.models.base import Base
from src.settings import DATABASE_URL, POOL_SIZE, MAX_OVERFLOW


def main():
    connect_args = {
        'application_name': "CreateTables"
    }
    engine = create_engine(DATABASE_URL, pool_size=POOL_SIZE, max_overflow=MAX_OVERFLOW, connect_args=connect_args)

    session = Session(engine)
    with session.begin():
        session.execute(text('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'))
    Base.metadata.create_all(engine)


if __name__ == '__main__':
    main()
