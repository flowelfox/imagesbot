#!/home/flowelcat/PycharmProjects/spacegame/.venv/bin/python
# -*- coding: utf-8 -*-
import re
import sys
from src.scripts.alembic import CommandLine, Config


class CustomCommandLine(CommandLine):
    def main(self, argv=None):
        options = self.parser.parse_args(argv)
        if not hasattr(options, "cmd"):
            self.parser.error("too few arguments")
        else:
            cfg = Config(cmd_opts=options)
            cfg.set_main_option("script_location", "marketplace.database:migrations")
            cfg.set_main_option("sqlalchemy.url", "postgresql://foo/bar")
            cfg.set_main_option("file_template", "%%(epoch)s.%%(year)d-%%(month).2d-%%(day).2d_%%(slug)s")
            self.run_cmd(cfg, options)


def main(argv=None, prog=None, **kwargs):
    """The console runner function for Alembic."""
    CustomCommandLine(prog=prog).main(argv=argv)


if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())
