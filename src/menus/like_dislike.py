import logging

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import TelegramError, BadRequest

from src.database.enums import ReactionType
from src.database.models import Reaction, Post

logger = logging.getLogger(__name__)


async def post_like(update, context):
    DBSession = context.session

    post_id = update.callback_query.data.replace("like_post_", "")
    post = DBSession.query(Post).get(post_id)
    if post is None:
        logger.warning(f"Can't find post with id {post_id}")
        await update.callback_query.answer()
        return

    existed_like = DBSession.query(Reaction)\
        .filter(Reaction.post_id == post_id)\
        .filter(Reaction.type == ReactionType.like)\
        .filter(Reaction.user_chat_id == update.callback_query.from_user.id)\
        .first()

    if existed_like:
        DBSession.delete(existed_like)
    else:
        existed_dislike = DBSession.query(Reaction)\
            .filter(Reaction.post_id == post_id)\
            .filter(Reaction.type == ReactionType.dislike)\
            .filter(Reaction.user_chat_id == update.callback_query.from_user.id)\
            .first()
        if existed_dislike:
            DBSession.delete(existed_dislike)

        like = Reaction(type=ReactionType.like, user_chat_id=update.callback_query.from_user.id, post_id=post_id)
        DBSession.add(like)

    DBSession.commit()
    post_likes = post.likes()
    post_dislikes = post.dislikes()

    buttons = [[InlineKeyboardButton(f"♥ {post_likes}", callback_data=f"like_post_{post.id}"),
                InlineKeyboardButton(f"💔 {post_dislikes}", callback_data=f"dislike_post_{post.id}")]]
    try:
        await update.effective_message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(buttons))
    except TelegramError:
        pass
    try:
        await update.callback_query.answer()
    except BadRequest:
        pass


async def post_dislike(update, context):
    DBSession = context.session

    post_id = update.callback_query.data.replace("dislike_post_", "")
    post = DBSession.query(Post).get(post_id)
    if post is None:
        logger.warning(f"Can't find post with id {post_id}")
        await update.callback_query.answer()
        return

    existed_dislike = DBSession.query(Reaction)\
        .filter(Reaction.post_id == post_id)\
        .filter(Reaction.type == ReactionType.dislike)\
        .filter(Reaction.user_chat_id == update.callback_query.from_user.id)\
        .first()
    if existed_dislike:
        DBSession.delete(existed_dislike)
    else:
        existed_like = DBSession.query(Reaction)\
            .filter(Reaction.post_id == post_id)\
            .filter(Reaction.type == ReactionType.like)\
            .filter(Reaction.user_chat_id == update.callback_query.from_user.id)\
            .first()
        if existed_like:
            DBSession.delete(existed_like)

        dislike = Reaction(type=ReactionType.dislike, user_chat_id=update.callback_query.from_user.id, post_id=post_id)
        DBSession.add(dislike)

    DBSession.commit()
    post_likes = post.likes()
    post_dislikes = post.dislikes()

    buttons = [[InlineKeyboardButton(f"♥ {post_likes}", callback_data=f"like_post_{post.id}"),
                InlineKeyboardButton(f"💔 {post_dislikes}", callback_data=f"dislike_post_{post.id}")]]
    try:
        await update.effective_message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(buttons))
    except TelegramError:
        pass
    try:
        await update.callback_query.answer()
    except BadRequest:
        pass
