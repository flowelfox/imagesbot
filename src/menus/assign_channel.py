from enum import Enum

from formencode import validators, Invalid
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, MessageHandler, BaseHandler, filters

from src.lib.basemenu import BaseMenu
from src.lib.helpers import get_settings, write_settings
from src.lib.messages import send_or_edit, delete_interface
from src.settings import SETTINGS_FILE


class AssignChannel(BaseMenu):
    class States(Enum):
        DEFAULT = 1
        LINK = 2

    async def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        await self.send_message(context)
        return self.States.DEFAULT

    async def send_message(self, context):
        _ = context.user.translator

        settings = await get_settings(SETTINGS_FILE)

        message_text = _("Channel") + f": {settings['channel']['title']}\n"
        message_text += _("ID") + f": {settings['channel']['id']}\n"
        message_text += _("Link") + f": {settings['channel']['link']}\n\n"

        message_text += _("Please forward me message from channel to set new channel.")

        buttons = [[InlineKeyboardButton(_("🔗 Set link"), callback_data="set_link")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        await send_or_edit(context, chat_id=context.user.id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    async def ask_link(self, update, context):
        _ = context.user.translator

        message_text = _("Please send me channel invite link")
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
        await send_or_edit(context, chat_id=context.user.id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.LINK

    async def set_link(self, update, context):
        _ = context.user.translator

        try:
            val = validators.URL()
            link = val.to_python(update.effective_message.text)
            channel = (await get_settings(SETTINGS_FILE))['channel']
            channel['link'] = link
            await write_settings({"channel": channel}, SETTINGS_FILE)
            message_text = _("New channel link was set")
        except Invalid:
            message_text = _("Wrong format, please write and send me invite link to channel")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
        await delete_interface(context)
        await send_or_edit(context, chat_id=context.user.id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LINK

    async def set_channel(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        channel = {"id": update.effective_message.forward_from_chat.id,
                   "title": update.effective_message.forward_from_chat.title,
                   "link": "google.com"}

        await write_settings({"channel": channel}, SETTINGS_FILE)

        message_text = _("New channel assigned")
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
        await delete_interface(context)
        await send_or_edit(context, chat_id=context.user.id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.DEFAULT

    def entry_points(self) -> list[BaseHandler]:
        return [CallbackQueryHandler(self.entry, pattern='^assign_channel$')]


    def states(self) -> dict[Enum, list[BaseHandler]]:
       return {
            self.States.DEFAULT: [CallbackQueryHandler(self.ask_link, pattern=f"^set_link$"),
                                 CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                 CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$"),
                                 MessageHandler(filters.FORWARDED, self.set_channel)],
            self.States.LINK: [MessageHandler(filters.TEXT, self.set_link),
                               CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$")]
        }