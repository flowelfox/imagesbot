from enum import Enum

from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, PrefixHandler, filters, BaseHandler

from src.lib.basemenu import BaseMenu
from src.lib.helpers import prepare_user, get_settings
from src.lib.messages import delete_interface, send_or_edit, delete_user_message
from src.menus.assign_channel import AssignChannel
from src.menus.load_images import LoadImages
from src.menus.posting_time import PostingTime
from src.settings import SUPERUSER_ACCOUNTS, SETTINGS_FILE


class StartMenu(BaseMenu):
    async def entry(self, update, context):
        user = await prepare_user(update, context)

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.effective_message and update.effective_message.text == "/start":
            await delete_interface(context)

        if context.user.id not in SUPERUSER_ACCOUNTS:
            await self.send_restrict_message(context)
            return self.States.DEFAULT

        await self.send_message(context)
        return self.States.DEFAULT

    async def send_restrict_message(self, context):
        _ = context.user.translator

        await send_or_edit(context, chat_id=context.user.id, text=_("Sorry, this is private bot. You are not allowed to use this bot."))

    async def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        settings = await get_settings(SETTINGS_FILE)
        buttons = [[InlineKeyboardButton(_("🖼 Load images"), callback_data="load_images")],
                   [InlineKeyboardButton(_("⏳ Posting time"), callback_data="posting_time")],
                   [InlineKeyboardButton(_("🔗 Assign channel"), callback_data="assign_channel")]]

        if settings['channel']['id'] != 0:
            buttons.append([InlineKeyboardButton(settings['channel']['title'], url=settings['channel']['link'])])

        await send_or_edit(context, chat_id=context.user.id, text=_("Select section"), reply_markup=InlineKeyboardMarkup(buttons))

    def entry_points(self) -> list[BaseHandler]:
        return [PrefixHandler('/', 'start', self.entry), CallbackQueryHandler(self.entry, pattern='^start$')]


    def states(self) -> dict[Enum, list[BaseHandler]]:
        return {
                                          self.States.DEFAULT: [AssignChannel(self).handler,
                                                               LoadImages(self).handler,
                                                               PostingTime(self).handler],
                                      }

    def fallbacks(self) -> list[BaseHandler]:
        return [MessageHandler(filters.ALL, lambda u, c: delete_user_message(u))]
