from enum import Enum
from io import BytesIO

import imagehash
from PIL import Image
from sqlalchemy import func
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, MessageHandler, filters, BaseHandler

from src.database.models import Image as ImageModel, User
from src.lib.basemenu import BaseMenu
from src.lib.messages import send_or_edit, delete_user_message
from src.settings import IMAGE_WAIT_TIME


class LoadImages(BaseMenu):
    async def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['images'] = []
        context.user_data[self.menu_name]['skipped_images'] = []
        await self.send_message(context)
        return self.States.DEFAULT

    async def send_message(self, context):
        _ = context.user.translator

        images_count = context.session.query(func.count(ImageModel.id)).scalar()
        used_images = context.session.query(func.count(ImageModel.id)).filter(ImageModel.post != None).scalar()
        unused_images = context.session.query(func.count(ImageModel.id)).filter(ImageModel.post == None).scalar()

        message_text = _("There is currently {images_count} images in bot.").format(images_count=images_count) + "\n"
        message_text += _("{used_images} used images").format(used_images=used_images) + "\n"
        message_text += _("{unused_images} unused images").format(unused_images=unused_images) + "\n\n"

        message_text += _("Please send me photos.")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))

    async def load_images(self, update, context):
        _ = context.user.translator

        images = context.user_data[self.menu_name]['images']
        context.user_data[self.menu_name]['skipped_images'] = []

        mes = await send_or_edit(context, chat_id=context.user.id, text=_("Uploading images, please wait..."))

        file_id = update.effective_message.photo[-1].file_id
        file_unique_id = update.effective_message.photo[-1].file_unique_id
        file_data = await (await context.bot.get_file(file_id)).download_as_bytearray()
        hash = str(imagehash.average_hash(Image.open(BytesIO(file_data))))

        images.append(ImageModel(file_id=file_id, file_unique_id=file_unique_id, hash=hash))

        upload_jobs = context.job_queue.get_jobs_by_name("upload_job")
        for upload_job in upload_jobs:
            upload_job.schedule_removal()

        context.job_queue.run_once(self.upload_images_job, IMAGE_WAIT_TIME, data={"images": images,
                                                                                  "user_id": context.user.id,
                                                                                  "skipped_images":
                                                                                      context.user_data[self.menu_name][
                                                                                          'skipped_images'],
                                                                                  "message_id": mes.message_id},
                                   name="upload_job")

        await delete_user_message(update)
        return self.States.DEFAULT

    async def upload_images_job(self, context):

        images = context.job.data['images']
        user = context.session.query(User).get(context.job.data['user_id'])
        skipped_images = context.job.data['skipped_images']
        message_id = context.job.data['message_id']
        _ = user.translator

        imported = 0
        for image in images:
            try:
                context.session.merge(image)
                context.session.commit()
                imported += 1
            except Exception as e:
                context.session.rollback()
                existing_image_query = context.session.query(ImageModel)
                if 'file_unique_id' in str(e):
                    existing_image_query = existing_image_query.filter(
                        ImageModel.file_unique_id == image.file_unique_id)
                elif 'hash' in str(e):
                    existing_image_query = existing_image_query.filter(ImageModel.hash == image.hash)

                existing_image = existing_image_query.first()
                skipped_images.append(existing_image)

        message_text = _("Loaded {loaded} images.").format(loaded=len(images)) + "\n"
        message_text += _("Uploaded {imported} images.").format(imported=imported) + "\n"
        message_text += _("Skipped {skipped} images.").format(skipped=len(skipped_images)) + "\n"

        buttons = []
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")])

        await context.bot.edit_message_text(message_id=message_id,
                                            chat_id=user.id,
                                            text=message_text,
                                            reply_markup=InlineKeyboardMarkup(buttons))
        images.clear()

    def entry_points(self) -> list[BaseHandler]:
        return [CallbackQueryHandler(self.entry, pattern='^load_images$')]

    def states(self) -> dict[Enum, list[BaseHandler]]:
        return {
            self.States.DEFAULT: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                  CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                  MessageHandler(filters.PHOTO, self.load_images)],
        }
