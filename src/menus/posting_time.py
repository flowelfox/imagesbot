from enum import Enum

from formencode import Invalid, validators
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, MessageHandler, BaseHandler, filters

from src.lib.basemenu import BaseMenu
from src.lib.helpers import get_settings, write_settings, group_buttons
from src.lib.messages import send_or_edit, delete_interface
from src.settings import SETTINGS_FILE


class PostingTime(BaseMenu):
    class States(Enum):
        DEFAULT = 1
        ADD_POSTING_TIME = 2
        REMOVE_POSTING_TIME = 3

    async def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        await self.send_message(context)
        return self.States.DEFAULT

    async def send_message(self, context):
        _ = context.user.translator

        settings = await get_settings(SETTINGS_FILE)
        posting_time = settings["posting_time"]

        message_text = _("Post will be sent at that time:") + "\n"
        for pt in posting_time:
            message_text += f"{pt}\n"

        buttons = [[InlineKeyboardButton(_("➕ Add"), callback_data="add_posting_time"),
                    InlineKeyboardButton(_("➖ Remove"), callback_data="remove_posting_time")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))

    async def ask_add_posting_time(self, update, context):
        _ = context.user.translator

        message_text = _("Please enter new posting time in HH:MM format")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_menu")]]
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.ADD_POSTING_TIME

    async def add_posting_time(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        settings = await get_settings(SETTINGS_FILE)
        posting_time = settings["posting_time"]
        try:
            val = validators.TimeConverter()
            hour, minute = val.to_python(update.effective_message.text)
            new_time_str = f"{hour:02}:{minute:02}"
            if new_time_str not in posting_time:
                posting_time.append(new_time_str)
                posting_time = list(sorted(posting_time))
                await write_settings({"posting_time": posting_time}, SETTINGS_FILE)

            message_text = _(
                "New posting time added, if you want to add another please write and send it in HH:MM format")
        except Invalid:
            message_text = _("Wrong time format. Please send me time in HH:MM format")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_menu")]]
        await delete_interface(context)
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ADD_POSTING_TIME

    async def ask_remove_posting_time(self, update, context):
        _ = context.user.translator

        message_text = _("Please select posting time to remove")

        settings = await get_settings(SETTINGS_FILE)
        posting_time = settings["posting_time"]

        posting_time_buttons = []
        for pt in posting_time:
            posting_time_buttons.append(InlineKeyboardButton(pt, callback_data=f"remove_{pt}"))

        buttons = group_buttons(posting_time_buttons, 4)
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_menu")])
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.REMOVE_POSTING_TIME

    async def remove_posting_time(self, update, context):
        _ = context.user.translator

        posting_time_to_remove = update.callback_query.data.replace("remove_", "")
        settings = await get_settings(SETTINGS_FILE)
        posting_time = settings["posting_time"]

        if posting_time_to_remove in posting_time:
            posting_time.remove(posting_time_to_remove)
            posting_time = list(sorted(posting_time))
            await write_settings({"posting_time": posting_time}, SETTINGS_FILE)

        message_text = _("Posting time removed, if you want to remove another please select it using buttons below")

        posting_time_buttons = []
        for pt in posting_time:
            posting_time_buttons.append(InlineKeyboardButton(pt, callback_data=f"remove_{pt}"))

        buttons = group_buttons(posting_time_buttons, 4)
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_menu")])
        await send_or_edit(context, chat_id=context.user.id, text=message_text,
                           reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.REMOVE_POSTING_TIME

    def entry_points(self) -> list[BaseHandler]:
        return [CallbackQueryHandler(self.entry, pattern='^posting_time$')]

    def states(self) -> dict[Enum, list[BaseHandler]]:
        return {
            self.States.DEFAULT: [CallbackQueryHandler(self.ask_add_posting_time, pattern=f"^add_posting_time$"),
                                  CallbackQueryHandler(self.ask_remove_posting_time, pattern=f"^remove_posting_time$"),
                                  CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                  CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$")],
            self.States.ADD_POSTING_TIME: [MessageHandler(filters.TEXT, self.add_posting_time),
                                           CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$")],
            self.States.REMOVE_POSTING_TIME: [
                CallbackQueryHandler(self.remove_posting_time, pattern=r"^remove_\d+:\d+$"),
                CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$")]
        }
