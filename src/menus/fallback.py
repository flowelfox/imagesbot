from telegram import Update
from telegram.error import BadRequest
from telegram.ext import ContextTypes, ConversationHandler, MessageHandler, filters, CallbackQueryHandler

from src.lib.basemenu import States


def get_conversation_handlers(handlers):
    result = []
    for handler in filter(lambda h: isinstance(h, ConversationHandler), handlers):
        result.append(handler)
        for inner_handlers in handler.states.values():
            result.extend(get_conversation_handlers(inner_handlers))
    return result


async def goto_start(update: Update, context: ContextTypes.DEFAULT_TYPE):

    if update.effective_message:
        try:
            await update.effective_message.delete()
        except BadRequest:
            pass

    handlers = get_conversation_handlers(context.application.handlers[0])
    start_menu_handler = None
    user_id_pair = (update.effective_chat.id, update.effective_chat.id)
    for handler in handlers:
        if handler.name == "start_menu":
            start_menu_handler = handler

        if user_id_pair in handler._conversations:
            handler._conversations.pop(user_id_pair)

    start_menu_handler._conversations.update({(user_id_pair): States.DEFAULT})
    await start_menu_handler._entry_points[0].callback(update, context)


fallback_message_handler = MessageHandler(filters.ALL, goto_start)
fallback_query_handler = CallbackQueryHandler(goto_start)
