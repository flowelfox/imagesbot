import gettext
import json
import logging
import os
import re
from io import BytesIO
from typing import Optional, Callable, Union

from PIL import Image
from sqlalchemy import Update, select
from telegram import InlineKeyboardButton, KeyboardButton, Bot
from telegram.error import Forbidden
from telegram.ext import ContextTypes, CallbackContext

from src.database.models import User
from src.settings import PROJECT_ROOT, SETTINGS_FILE

logger = logging.getLogger(__name__)


def available_languages():
    locales_folder = PROJECT_ROOT / 'locales'
    if locales_folder.exists():
        languages = next(locales_folder)[1]
    else:
        languages = []
    if 'en' not in languages:
        languages.append('en')

    return languages


def translator(language_code='en'):
    translation = gettext.translation(
        'messages', str(PROJECT_ROOT / 'locales'), languages=[language_code], fallback=True
    )
    return translation.gettext


def ntranslator(language_code='en'):
    translation = gettext.translation(
        'messages', str(PROJECT_ROOT / 'locales'), languages=[language_code], fallback=True
    )
    return translation.ngettext


async def prepare_user(update: Update, context: ContextTypes.DEFAULT_TYPE, lang: Optional[str] = None) -> User:
    if context.user_data.get('user', False) and context.user_data.get('_', False):
        user = context.user_data['user']
        user.activate()
        user.save()
        return user

    with context.session.begin():
        stmt = select(User).where(User.id == update.effective_user.id)
        user = context.session.scalar(stmt)
        tuser = update.effective_user
        if lang is None and tuser.language_code:
            lang = tuser.language_code
        elif lang is None and tuser.language_code is None:
            lang = 'en'

        # Create new user if it is not exists
        if not user:
            came_from = context.args[0] if context.args else None
            user = User()
            user.id = tuser.id
            user.first_name = tuser.first_name
            user.last_name = tuser.last_name
            user.username = tuser.username
            user.came_from = came_from
            logger.info(f'New user joined bot: "{user.name}".')

            if hasattr(user, 'init_permissions'):
                user.init_permissions(context.session)

            try:
                if came_from and came_from.startswith("invite-"):
                    referrer = (
                        await
                        context.session.scalars(select(User).where(User.id == int(came_from.replace("invite-", ""))))
                    ).one()
                    if referrer and referrer.account:
                        user.referrer = referrer
                        _ = translator(lang)

                        referrer_text = _("You were invited by {user}.").format(
                            user=f"<a href=\"{referrer.mention_url}\">{referrer.name}</a>"
                        )
                        referral_text = _("You invited {user}.").format(
                            user=f"<a href=\"{user.mention_url}\">{user.name}</a>"
                        )
                        context.bot.send_message(chat_id=tuser.id, text=referrer_text, parse_mode="HTML")
                        context.bot.send_message(chat_id=referrer.id, text=referral_text, parse_mode="HTML")
            except ValueError:
                pass
            except Forbidden:
                logger.warning("Can't send message to user during account initialization")

        # update user data if changed
        user.first_name = tuser.first_name
        user.last_name = tuser.last_name
        user.username = tuser.username
        user.activate()
        if user.language_code is None:
            user.language_code = lang

        context.session.add(user)
        context.session.flush()
        user = context.session.scalar(stmt)
        context.session.expunge(user)

    context.user_data['user'] = user
    context.user = user
    return user


async def write_settings(new_data: dict, settings_path, recreate=False):
    if not recreate:
        data = await get_settings(settings_path)
        data.update(new_data)
    else:
        data = new_data

    with open(settings_path, 'w') as f:
        json.dump(data, f)


async def get_settings(settings_path):
    if not os.path.exists(settings_path):
        return {}
    with open(settings_path, 'r') as f:
        data = json.load(f)
    return data


def get_photo(*args):
    if not SETTINGS_FILE.exists():
        return {}
    with open(SETTINGS_FILE, 'r') as f:
        data = json.load(f)
    images = data['images']

    try:
        part = images
        for arg in args:
            part = part[arg]
        return part
    except KeyError:
        return images['others']['unknown']


def pascal_to_snake(name: str):
    name = re.sub(r'(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', name).lower()


def init_user_data(func: Callable):
    def inner(*args, **kwargs):
        context = None
        self = None
        for arg in args + tuple(kwargs.values()):
            if isinstance(arg, CallbackContext):
                context = arg
            if hasattr(arg, "menu_name"):
                self = arg

        if context and self:
            if self.menu_name not in context.user_data:
                context.user_data[self.menu_name] = {}

        return func(*args, **kwargs)

    return inner


def group_buttons(buttons: Union[list[InlineKeyboardButton], list[KeyboardButton]], group_size: int = 2) \
        -> list[list[Union[InlineKeyboardButton, KeyboardButton]]]:
    group = []
    subgroup = []
    for button in buttons:
        subgroup.append(button)
        if len(subgroup) == group_size:
            group.append(subgroup)
            subgroup = []

    if subgroup:
        group.append(subgroup)

    return group


def callback_placeholder(state, text: str = None, show_alert: bool = True):
    async def placeholder(update: Update, context: CallbackContext):
        await context.bot.answer_callback_query(update.callback_query.id, text=text, show_alert=show_alert)
        return state

    return placeholder


def resize_image_width(image, size, resample=Image.LANCZOS):
    """Resize image according to size.

    image:      a Pillow image instance
    size:       an integer or a list or tuple of two integers [width, height]
    """
    try:
        width = size[0]
    except TypeError:
        width = size
    img_format = image.format
    img = image.copy()
    img_size = img.size
    # If the origial image has already the good width, return it
    # fix issue #16
    if img_size[0] == width:
        return image

    wpercent = (width / float(img.size[0]))
    new_height = int((float(img.size[1]) * float(wpercent)))

    img = img.resize((width, new_height), resample)
    img.format = img_format
    return img


def escape_markdown(text: str):
    escape_chars = r"\_*[]()~`>#+-=|{}.!"
    for char in escape_chars:
        text = text.replace(char, fr"\{char}")
    return text


async def resize_cover(bot: Bot, file_id: str):
    telegram_file = await bot.get_file(file_id)
    downloaded_file = await telegram_file.download_as_bytearray()
    orig_photo_file = BytesIO(downloaded_file)

    resized_photo_file = BytesIO()
    resize_image_width(Image.open(orig_photo_file), 512).save(resized_photo_file, format="JPEG")
    resized_photo_file.name = "cover.jpg"
    resized_photo_file.seek(0)
    return resized_photo_file
