from abc import ABCMeta
from typing import Any

from cerberus import Validator as CerberusValidator

from marketplace.database.enums import ProductCondition
from marketplace.lib.helpers import translator


class Validator(metaclass=ABCMeta):
    def __init__(self, schema: dict, base=CerberusValidator):
        self.field_name = list(schema.keys())[0]
        self._validator = base(schema, require_all=True)

    def validate(self, value: str) -> bool:
        raise NotImplementedError

    def validated(self, value: str, default: Any) -> Any:
        raise NotImplementedError

    def normalize(self, value: str, default: Any) -> Any:
        raise NotImplementedError

    def error_messages(self, lang: str):
        errors = self._validator.document_error_tree.descendants[self.field_name].errors
        for error in errors:
            message_raw = self.get_message(error.code, lang)
            yield message_raw.format(*error.info, constraint=error.constraint, field=self.field_name, value=error.value)

    def get_message(self, code: int, lang: str):
        _ = translator(lang)
        messages = {
            0x24: _("Wrong value type"),
            0x27: _("Minimal length is {constraint} characters"),
            0x28: _("Maximum length is {constraint} characters"),
            0x42: _("Minimum value is {constraint}"),
            0x43: _("Maximum value is {constraint}"),
        }
        return messages.get(code, _("Unknown error"))


def validator_factory(schema: dict, base=CerberusValidator):
    class NewValidator(Validator):
        def validate(self, value: str):
            return self._validator.validate({self.field_name: value})

        def validated(self, value: str, default: str = None):
            validated_document = self._validator.validated({self.field_name: value})
            if validated_document is not None:
                return validated_document.get(self.field_name, default)

        def normalize(self, value: str, default: str = None):
            return self._validator.normalized({
                self.field_name: value
            }).get(self.field_name, default)

    return NewValidator(schema, base)


def clear_float(value: str) -> str:
    return value.replace("\\", "").replace(",", ".")


product_title_schema = {
    "title": {
        "type": "string",
        "minlength": 3,
        "maxlength": 64
    }
}

product_description_schema = {
    "description": {
        "type": "string",
        "minlength": 3,
        "maxlength": 2048
    }
}

product_price_schema = {
    "price": {
        "type": "integer",
        "coerce": (clear_float, float, round, int),
        "min": 0,
        "max": 1000000000,
    }
}


class ProductConditionCerberusValidator(CerberusValidator):
    def _validate_type_product_condition(self, value):
        if isinstance(value, ProductCondition):
            return True


product_condition_schema = {
    "condition": {
        "type": "product_condition",
    }
}
