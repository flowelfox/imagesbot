import html
import logging
import traceback
from copy import deepcopy
from html.parser import HTMLParser
from io import StringIO
from typing import Union, Optional

from telegram import InlineKeyboardMarkup, Update, InputMedia, Message, Bot
from telegram import InputFile
from telegram import InputMediaAnimation
from telegram import InputMediaDocument
from telegram import InputMediaPhoto
from telegram import InputMediaVideo
from telegram import ReplyKeyboardMarkup
from telegram.error import TelegramError
from telegram.error import BadRequest
from telegram.ext import CallbackContext

logger = logging.getLogger("sender")


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_data(self, d):
        self.text.write(d)

    def get_data(self):
        return self.text.getvalue()


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


class Interface:
    """This class represents telegram message interface.

    Usually user must interact only with one last telegram message. This
    class store message, keyboard and other useful info about telegram
    message
    """
    def __init__(self, name, message=None):
        self.name = name
        self._message = message
        self.reply_markup = None
        self.parse_mode = None
        self.disable_web_page_preview = None
        self.disable_notification = None
        self.media = None

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value: Message):
        self._message = value

    def __getstate__(self):
        return vars(self)

    def __setstate__(self, state):
        vars(self).update(state)

    def __getattr__(self, attr: str):
        if attr == "_message":
            result = self.__dict__.get("_message", None)
            if result is None:
                raise AttributeError(f"'{self.__class__.__name__}' object has no attribute '{attr}'")
            return result
        elif "_message" in self.__dict__:
            return getattr(self._message, attr)
        else:
            raise AttributeError(f"'{self.__class__.__name__}' object has no attribute '{attr}'")

    def __deepcopy__(self, memo: dict):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))

        return result

    def extend(self, data: dict):
        self.reply_markup = data.get('reply_markup', None)
        self.parse_mode = data.get('parse_mode', None)
        self.disable_web_page_preview = data.get('disable_web_page_preview', None)
        self.disable_notification = data.get('disable_notification', None)
        photo = data.get("photo", None)
        document = data.get("document", None)
        video = data.get("video", None)
        animation = data.get("animation", None)
        if photo is not None:
            # check if media is file
            if hasattr(photo, 'read'):
                photo.seek(0)
            self.media = InputMediaPhoto(photo, self.caption, self.parse_mode)
        if document is not None:
            # check if media is file
            if hasattr(document, 'read'):
                document.seek(0)
            self.media = InputMediaDocument(document, self.caption, self.parse_mode)
        if video is not None:
            # check if media is file
            if hasattr(video, 'read'):
                video.seek(0)
            self.media = InputMediaVideo(video, self.caption, self.parse_mode)
        if animation is not None:
            # check if media is file
            if hasattr(animation, 'read'):
                animation.seek(0)
            self.media = InputMediaAnimation(animation, self.caption, self.parse_mode)

    def edit_media(self, media: InputMedia, **kwargs):
        self.media = media
        new_kwargs = {}
        for param in ['chat_id', 'message_id', 'inline_message_id', 'reply_markup', 'timeout', 'api_kwargs']:
            if param in kwargs:
                new_kwargs.update({param: kwargs[param]})

        return self.message.edit_media(media, **new_kwargs)

    async def resend(self, bot: Bot):
        if self.message is not None:
            try:
                await self.message.delete()
            except BadRequest:
                pass

            kwargs = {
                'chat_id': self.chat_id,
                'text': self.text,
                'parse_mode': self.parse_mode,
                'disable_web_page_preview': self.disable_web_page_preview,
                'disable_notification': self.disable_notification,
                'reply_markup': self.reply_markup,
                'photo': self.photo[-1].file_id if self.photo else None,
                'caption': self.caption,
                'document': self.document,
                'video': self.video,
                'animation': self.animation,
                'reply_to_message_id': self.reply_to_message.id if self.reply_to_message else None
            }
            kwargs = {
                k: v
                for k, v in kwargs.items() if v is not None
            }
            self.message = await _send_telegram_message(bot, **kwargs)

    @property
    def reply_markup_type(self):
        if self.reply_markup:
            return self.reply_markup.__class__

    def save(self, user_data: dict):
        """Save interface to user_data.

        Args:
            user_data (:str:) User data from context where interface would be saved
        """
        user_data['interfaces'][self.name] = self


async def _send_telegram_message(bot: Bot, **kwargs) -> Message:
    if 'text' in kwargs:
        return await bot.send_message(**kwargs)
    elif 'photo' in kwargs:
        try:
            if not isinstance(kwargs['photo'], str):
                kwargs['photo'].seek(0)

            if 'disable_web_page_preview' in kwargs:
                kwargs.pop('disable_web_page_preview')
            return await bot.send_photo(**kwargs)
        except BadRequest as exc:
            # try send without tags
            if exc.message == "Media_caption_too_long" and 'caption' in kwargs and kwargs['caption'] is not None:
                try:
                    kwargs['caption'] = strip_tags(kwargs['caption'])
                    return await bot.send_photo(**kwargs)
                except BadRequest:
                    # send cropped
                    logger.debug(f"Error while sending photo: \n{traceback.format_exc()}")
                    kwargs['caption'] = kwargs['caption'][:1020] + "✂️"
                    return await bot.send_photo(**kwargs)
            else:
                raise exc
        finally:
            if not isinstance(kwargs['photo'], str):
                kwargs['photo'].seek(0)

    elif 'animation' in kwargs:
        return await bot.send_animation(**kwargs)
    elif 'video' in kwargs:
        return await bot.send_video(**kwargs)
    elif 'document' in kwargs:
        return await bot.send_document(**kwargs)
    elif 'location' in kwargs:
        return await bot.send_location(**kwargs)
    elif 'sticker' in kwargs:
        return await bot.send_sticker(**kwargs)


def _init_interfaces(user_data):
    if 'interfaces' not in user_data:
        user_data['interfaces'] = {}


def get_user_data(application, chat_id: str) -> dict:
    key = chat_id
    user_data = application.user_data.get(key, None)
    if user_data is None:
        application.user_data[key] = {}
        user_data = application.user_data[key]

    return user_data


async def send_or_edit(
    context: CallbackContext,
    interface_name: Optional[str] = 'interface',
    user_id: str = None,
    application=None,
    **kwargs
) -> Union[Interface, None]:
    """Use this function to send any telegram text,document,photo or location
    messages.

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be later used
        to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
        kwargs (:obj:`dict`, optional): Any keyword arguments which will be passed to bot.send_message method.
    """
    if user_id and application is None:
        logger.error("You must pass dispatcher with user_id")
        return None
    if user_id and application:
        user_data = get_user_data(application, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return None
    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, Interface(interface_name))

    if len(kwargs.get('text', '')) > 4090:
        chunk_size = 4090
        chunks = len(kwargs['text'])
        message_list = [kwargs['text'][i:i + chunk_size] for i in range(0, chunks, chunk_size)]

        if 'reply_markup' in kwargs:
            reply_markup = kwargs['reply_markup']
            kwargs['reply_markup'] = None
        else:
            reply_markup = None

        for idx, message in enumerate(message_list):
            kwargs['text'] = message
            if idx == len(message_list) - 1:
                kwargs['reply_markup'] = reply_markup

            interface.message = await _send_telegram_message(context.bot, **kwargs)
            interface.extend(kwargs)
            interface.save(user_data)

        return interface

    if interface.message:
        new_reply_markup = kwargs.get('reply_markup', None)
        new_message_reply_markup_type = new_reply_markup.__class__ if new_reply_markup else None

        if interface.reply_markup_type is not None \
                and new_message_reply_markup_type is not None \
                and new_message_reply_markup_type is not interface.reply_markup_type:  # if reply_markup type differs
            await remove_interface_markup(context, interface_name)
            interface.message = await _send_telegram_message(context.bot, **kwargs)
        elif new_message_reply_markup_type is ReplyKeyboardMarkup:
            # if reply_markup is ReplyKeyboardMarkup always send new message
            """Please note, that it is currently only possible to edit messages
            without reply_markup or with inline keyboards."""
            interface.message = await _send_telegram_message(context.bot, **kwargs)
        elif (kwargs.get('reply_to_message_id', False) and not interface.reply_to_message) or \
                (kwargs.get('reply_to_message_id', False) and interface.reply_to_message
                 and kwargs.get('reply_to_message_id', False) != interface.reply_to_message.message_id):
            await delete_interface(context, interface_name)
            interface.message = await _send_telegram_message(context.bot, **kwargs)
            interface.save(user_data)
            return interface
        else:
            text_same = False
            markup_same = True

            # check is text changed
            if kwargs.get('photo', False) or kwargs.get('document', False):
                new_caption = kwargs.get('caption', '').strip(" \n\t")
                if (interface.caption_html and html.unescape(interface.caption_html) == new_caption) or \
                        (interface.caption_markdown_v2 and html.unescape(interface.caption_markdown_v2) == new_caption):
                    text_same = True
            else:
                new_text = kwargs.get('text', '').replace("<s>", "").replace("</s>", "").strip(" \n\t")
                if (interface.text_html and html.unescape(interface.text_html) == new_text) or \
                        (interface.text_markdown_v2 and html.unescape(interface.text_markdown_v2) == new_text):
                    text_same = True

            # check is buttons changed
            if new_message_reply_markup_type is InlineKeyboardMarkup:  # is markup in new message
                new_keyboard = new_reply_markup.inline_keyboard
                if interface.reply_markup:  # is markup in previous message
                    try:
                        for y, row in enumerate(interface.reply_markup.inline_keyboard):
                            for x, button in enumerate(row):
                                if new_keyboard[y][x].text != button.text or \
                                        (hasattr(new_keyboard[y][x], 'callback_data')
                                         and hasattr(button, 'callback_data')
                                         and new_keyboard[y][x].callback_data != button.callback_data) \
                                        or \
                                        (hasattr(new_keyboard[y][x], 'url')
                                         and hasattr(button, 'url')
                                         and new_keyboard[y][x].url != button.url):
                                    raise IndexError
                    except IndexError:
                        markup_same = False
                else:
                    markup_same = False
            elif new_message_reply_markup_type is ReplyKeyboardMarkup:  # is markup in new message
                new_keyboard = new_reply_markup.keyboard
                if interface.reply_markup:  # is markup in previous message
                    try:
                        for y, row in enumerate(interface.reply_markup.keyboard):
                            for x, button in enumerate(row):
                                if new_keyboard[y][x] != button:
                                    raise IndexError
                    except IndexError:
                        markup_same = False
                else:
                    markup_same = False

            # check is media changed
            if 'photo' in kwargs and interface.photo:
                media = InputMediaPhoto(
                    kwargs.get('photo', None), kwargs.get('caption', None), kwargs.get('parse_mode', None)
                )
            elif 'document' in kwargs and interface.document:
                media = InputMediaDocument(
                    kwargs.get('document', None), kwargs.get('caption', None), kwargs.get('parse_mode', None)
                )
            elif 'video' in kwargs and interface.video:
                media = InputMediaVideo(
                    kwargs.get('video', None), kwargs.get('caption', None), kwargs.get('parse_mode', None)
                )
            elif 'animation' in kwargs and interface.animation:
                media = InputMediaAnimation(
                    kwargs.get('animation', None), kwargs.get('caption', None), kwargs.get('parse_mode', None)
                )
            else:
                media = None

            if media and interface.media:
                if media.media == interface.media.media:
                    media_same = True
                elif media is None and interface.media.media is not None:
                    media_same = False
                elif type(media.media) != type(interface.media.media):  # noqa: E721
                    media_same = False
                elif isinstance(media.media, str) and isinstance(interface.media.media, str):
                    media_same = interface.media.media == media.media
                elif isinstance(media.media, InputFile) and isinstance(interface.media.media, InputFile):
                    media_same = media.media.input_file_content == interface.media.media.input_file_content
                else:
                    media_same = interface.media == media
            else:
                media_same = True

            if text_same and media_same and markup_same:
                interface.save(user_data)
                return interface
            elif text_same and media_same and not markup_same:
                interface.message = await interface.message.edit_reply_markup(reply_markup=new_reply_markup)
                interface.reply_markup = new_reply_markup
                interface.save(user_data)
                return interface

            try:
                text_changed = False
                media_changed = False
                if media is not None and not media_same:
                    interface.message = await interface.edit_media(
                        media=media, **{
                            k: v
                            for k, v in kwargs.items() if k != 'chat_id'
                        }
                    )
                    interface.reply_markup = new_reply_markup
                    media_changed = True
                elif 'text' in kwargs and interface.text and not text_same:
                    interface.message = await interface.message.edit_text(
                        **{
                            k: v
                            for k, v in kwargs.items() if k != 'chat_id'
                        }
                    )
                    interface.reply_markup = new_reply_markup
                    text_changed = True
                elif 'caption' in kwargs and interface.caption and not text_same:
                    interface.message = await interface.message.edit_caption(
                        **{
                            k: v
                            for k, v in kwargs.items() if k not in ('chat_id', 'photo', 'disable_web_page_preview')
                        }
                    )
                    interface.reply_markup = new_reply_markup
                    text_changed = True

                if text_changed or media_changed:
                    interface.save(user_data)
                    return interface

                # if message type changed
                if ('text' in kwargs and not interface.text) or \
                        ('caption' in kwargs and not interface.caption) or \
                        ('photo' in kwargs and not interface.photo) or \
                        ('document' in kwargs and not interface.document) or \
                        ('video' in kwargs and not interface.video) or \
                        ('animation' in kwargs and not interface.animation) or \
                        ('sticker' in kwargs) or \
                        ('location' in kwargs):
                    if interface.message:
                        await interface.message.delete()
                    interface.message = await _send_telegram_message(context.bot, **kwargs)
                else:
                    interface.message = await _send_telegram_message(context.bot, **kwargs)

            except (TelegramError, AttributeError) as exc:
                warning_text = f"Can't edit message: {exc}"
                if "Can't parse entities" in exc.message:
                    warning_text += "\nMessage text:\n"
                    warning_text += kwargs.get('text', kwargs.get('caption', ""))

                logger.warning(warning_text)
                interface.message = await _send_telegram_message(context.bot, **kwargs)

    else:
        await delete_interface(context, interface_name=interface_name)
        interface.message = await _send_telegram_message(context.bot, **kwargs)

    interface.extend(kwargs)
    interface.save(user_data)
    return interface


async def remove_interface(
    context: CallbackContext,
    interface_name: Optional[str] = 'interface',
    user_id: Optional[str] = None,
    application=None
):
    """Use this function to remove interface from user_data.

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be later used
        to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and application is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and application:
        user_data = get_user_data(application, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    if interface_name in user_data['interfaces']:
        del user_data['interfaces'][interface_name]


async def delete_interface(
    context: CallbackContext,
    interface_name: Optional[str] = 'interface',
    user_id: Optional[str] = None,
    application=None
):
    """Use this function to delete telegram message from user and remove
    interface from user_data."""
    if user_id and application is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and application:
        user_data = get_user_data(application, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)
    if interface and interface.message:
        try:
            await interface.message.delete()
        except (TelegramError, AttributeError) as e:
            logger.warning(f"Can't delete interface message: {e}")

    await remove_interface(context, interface_name, user_id, application)


async def remove_interface_markup(
    context: CallbackContext,
    interface_name: Optional[str] = 'interface',
    user_id: Optional[str] = None,
    dispatcher=None
):
    """Use this function to remove telegram message reply markup from telegram
    message."""
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)

    if interface:
        try:
            if interface.reply_markup and interface.reply_markup_type is ReplyKeyboardMarkup:
                await delete_interface(context, interface_name, user_id, dispatcher)
            else:
                await interface.message.edit_reply_markup()
        except (TelegramError, AttributeError) as e:
            logger.warning(f"Can't remove keyboard markup in interface message: {e}")

    await remove_interface(context, interface_name, user_id, dispatcher)


async def resend_interface(
    context: CallbackContext,
    interface_name: Optional[str] = 'interface',
    user_id: Optional[str] = None,
    dispatcher=None
):
    """Use this function to delete previous interface message and send same
    message agai.

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be later used
        to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)

    if interface is None:
        logger.debug("Can't resend user interface because it's not exist.")
        return
    await delete_interface(context, interface_name, user_id, dispatcher)

    await send_or_edit(
        context,
        interface_name,
        user_id,
        dispatcher,
        chat_id=interface.chat_id,
        text=interface.text,
        reply_markup=interface.reply_markup,
        disable_web_page_preview=interface.disable_web_page_preview,
        parse_mode=interface.parse_mode,
        disable_notification=interface.disable_notification
    )


def get_interface(context: CallbackContext, name: str):
    if 'interfaces' in context.user_data and name in context.user_data['interfaces']:
        return context.user_data['interfaces'][name]


async def delete_user_message(update: Update):
    """Use this function to delete message sent by user in update.

    Args:
        update(:py:class:`~telegram.Update`): Context from callback.
    """
    if update.message and update.message.message_id:
        await update.message.delete()
