import datetime
import re
from copy import copy
from dataclasses import dataclass
from enum import Enum, auto, EnumMeta
from itertools import cycle
from math import ceil
from typing import Optional, Callable, Union, Type

from cerberus import Validator as CerberusValidator
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton, PhotoSize, InputMediaPhoto
from telegram.constants import ParseMode, MessageType
from telegram.error import BadRequest
from telegram.ext import ContextTypes, CallbackQueryHandler, MessageHandler, filters, ConversationHandler

from marketplace.lib.basemenu import BaseMenu
from marketplace.lib.helpers import init_user_data, group_buttons, translator, escape_markdown
from marketplace.lib.messages import send_or_edit, delete_user_message, delete_interface
from marketplace.lib.validators import validator_factory


@dataclass
class Field:
    name: str
    title: str
    validator_schema: Optional[dict] = None
    validator_base_class: Optional[CerberusValidator] = CerberusValidator

    date_format: str = "%d.%m.%Y"
    datetime_format: str = "%H:%M %d.%m.%Y"
    time_format: str = "%H:%M"

    required: bool = False
    hidden: bool = False
    default: Optional[Union[str | list]] = None
    units: str = ""
    choices: Optional[Union[list | Type[Enum]]] = None
    choices_group_size: int = 5
    allow_multiple_choices: bool = False
    allow_multiple_photos: bool = False
    max_photos_count: int = 10
    depend_field_name: Optional[str] = None
    depend_field_value: Optional[str] = None
    depend_field_hides_current_field: bool = False
    before_validate: Optional[Callable] = None
    after_validate: Optional[Callable] = None
    message_type: MessageType = MessageType.TEXT
    callback: Optional[Callable] = None
    switchable: bool = True
    parse_mode: Optional[ParseMode] = None
    hint_text: Optional[str] = None
    allow_editor: bool = False

    def __post_init__(self):
        if isinstance(self.choices, EnumMeta):
            self.choices = list(self.choices)
        self.default = self.default if self.default is not None else ([] if self.allow_multiple_choices
                                                                      or self.allow_multiple_photos else None)
        self.value = self.default

    def validate(self, data):
        validator = validator_factory(self.validator_schema, self.validator_base_class)
        if self.before_validate is not None:
            data = self.before_validate(data)
        if not validator.validate(data):
            return ValueError(), validator
        value = validator.normalize(data)
        if self.after_validate is not None:
            value = self.after_validate(value)
        return value, validator

    def clean_value(self, language_code: str) -> str:
        _ = translator(language_code)

        if isinstance(self.value, str):
            clean_value = self.value.replace(r"\_", "_").replace(r"\*", "*").replace(r"\[", "[").replace(r"\]", "]")
        elif isinstance(self.value, int):
            clean_value = str(self.value)
        elif isinstance(self.value, float):
            clean_value = f"{self.value:.2f}"
        elif isinstance(self.value, datetime.datetime):
            clean_value = self.value.strftime(self.datetime_format)
        elif isinstance(self.value, datetime.date):
            clean_value = self.value.strftime(self.date_format)
        elif isinstance(self.value, datetime.time):
            clean_value = self.value.strftime(self.time_format)
        elif isinstance(self.value, list):
            if self.message_type == MessageType.PHOTO:
                clean_value = _("Uploaded") if self.value else _("Not uploaded")
                if self.allow_multiple_photos and self.value:
                    amount = escape_markdown(f" ({len(self.value)})")
                    clean_value += escape_markdown(amount) if self.parse_mode is ParseMode.MARKDOWN_V2 else amount
            else:
                clean_value = ', '.join(self.value)
        elif self.message_type is MessageType.PHOTO:
            clean_value = _("Uploaded") if self.value else _("Not uploaded")
        elif isinstance(self.value, bool):
            clean_value = _("Yes") if self.value else _("No")
        elif isinstance(self.value, Enum):
            if hasattr(self.value, "title"):
                clean_value = self.value.title(language_code)
            else:
                clean_value = str(self.value.value)
        else:
            clean_value = None
        return clean_value


class EditMenu(BaseMenu):
    class States(Enum):
        DEFAULT = auto()
        EDITOR = auto()

    parse_mode = ParseMode.HTML
    disable_web_page_preview = False
    show_field_selectors = False
    show_arrows = True
    field_buttons_group_size = 1
    choices_per_page = 5
    remove_user_messages = True

    async def entry(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        self.load(context)
        await self.send_message(context)
        return self.States.DEFAULT

    @init_user_data
    def load(self, context: ContextTypes.DEFAULT_TYPE):
        context.user_data[self.menu_name]['fields'] = {
            f.name: f
            for f in self.fields(context)
        }
        context.user_data[self.menu_name]['choices_page'] = 0
        context.user_data[self.menu_name]['editor_active'] = False
        context.user_data[self.menu_name]['alert_required_fields'] = False
        context.user_data[self.menu_name]['media_group_ids'] = []

        for field in context.user_data[self.menu_name]['fields'].values():
            if field.switchable:
                context.user_data[self.menu_name]['selected_field_name'] = field.name
                break

    def fields(self, context: ContextTypes.DEFAULT_TYPE) -> list[Field]:
        raise NotImplementedError

    def _get_field(self, context: ContextTypes.DEFAULT_TYPE, name: str) -> Optional[Field]:
        return context.user_data[self.menu_name]['fields'].get(name)

    def _get_selected_field(self, context: ContextTypes.DEFAULT_TYPE) -> Optional[Field]:
        return self._get_field(context, context.user_data[self.menu_name]['selected_field_name'])

    def _active_fields(self, context: ContextTypes.DEFAULT_TYPE) -> list[Field]:
        active_fields = []
        for field in context.user_data[self.menu_name]['fields'].values():
            depend_field = self._get_field(context, field.depend_field_name)
            if depend_field is None or (
                depend_field is not None and (
                    depend_field.value != field.depend_field_value
                    if field.depend_field_hides_current_field else depend_field.value == field.depend_field_value
                )
            ):
                active_fields.append(field)
        return active_fields

    def query_object(self, context: ContextTypes.DEFAULT_TYPE):
        raise NotImplementedError

    def previuous_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        return InlineKeyboardButton("🔼", callback_data=f"previous_field_{self.menu_name}")

    def next_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        return InlineKeyboardButton("🔽", callback_data=f"next_field_{self.menu_name}")

    def save_button(self, context: ContextTypes.DEFAULT_TYPE) -> Optional[InlineKeyboardButton]:
        _ = context.user.translator
        return InlineKeyboardButton(_("Save"), callback_data=f"save_{self.menu_name}")

    def edit_field_button(self, context: ContextTypes.DEFAULT_TYPE) -> Optional[InlineKeyboardButton]:
        _ = context.user.translator
        return InlineKeyboardButton(_("Edit"), callback_data=f"edit_field_{self.menu_name}")

    def back_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        _ = context.user.translator
        return InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")

    def back_from_editor_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        _ = context.user.translator
        return InlineKeyboardButton(_("Save and go back"), callback_data=f"exit_editor_{self.menu_name}")

    def cancel_editor_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        _ = context.user.translator
        return InlineKeyboardButton(_("Cancel"), callback_data=f"cancel_editor_{self.menu_name}")

    def bottom_buttons(self, context: ContextTypes.DEFAULT_TYPE) -> list[InlineKeyboardButton]:
        return []

    def clear_button(self, context: ContextTypes.DEFAULT_TYPE) -> Optional[InlineKeyboardButton]:
        _ = context.user.translator
        return InlineKeyboardButton(_("Clear"), callback_data=f"clear_field_{self.menu_name}")

    def controls(self, context: ContextTypes.DEFAULT_TYPE) -> list[list[InlineKeyboardButton]]:
        buttons = []
        if self.show_arrows:
            buttons.append(self.previuous_button(context))
        center_buttons = self.center_buttons(context)
        if center_buttons:
            buttons.extend(center_buttons)
        if self.show_arrows:
            buttons.append(self.next_button(context))
        return [buttons]

    def center_buttons(self, context: ContextTypes.DEFAULT_TYPE) -> list[InlineKeyboardButton]:
        buttons = []
        clear_button = self.clear_button(context)
        edit_field_button = self.edit_field_button(context)

        if edit_field_button:
            buttons.append(edit_field_button)

        if clear_button:
            buttons.append(clear_button)

        return buttons

    def previous_choice_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        return InlineKeyboardButton("\u21D0 Page", callback_data=f"previous_choice_{self.menu_name}")

    def next_choice_button(self, context: ContextTypes.DEFAULT_TYPE) -> InlineKeyboardButton:
        return InlineKeyboardButton("Page \u21D2", callback_data=f"next_choice_{self.menu_name}")

    def choices_center_buttons(self, context: ContextTypes.DEFAULT_TYPE) -> list[InlineKeyboardButton]:
        buttons = []
        return buttons

    def choices_controls(self, context: ContextTypes.DEFAULT_TYPE, choices: list) -> list[list[InlineKeyboardButton]]:
        buttons = []
        if len(choices) > self.choices_per_page:
            buttons.append(self.previous_choice_button(context))

        center_buttons = self.choices_center_buttons(context)
        if center_buttons:
            buttons.extend(center_buttons)

        if len(choices) > self.choices_per_page:
            buttons.append(self.next_choice_button(context))
        return [buttons]

    def message_top_text(self, context: ContextTypes.DEFAULT_TYPE) -> str:
        return ""

    def message_bottom_text(self, context: ContextTypes.DEFAULT_TYPE) -> str:
        selected_field = self._get_selected_field(context)
        if selected_field.hint_text:
            cursive_start_tag = "<i>" if self.parse_mode == ParseMode.HTML else "_"
            cursive_end_tag = "</i>" if self.parse_mode == ParseMode.HTML else "_"
            return cursive_start_tag + selected_field.hint_text + cursive_end_tag
        else:
            return ""

    def separator_text(self, context: ContextTypes.DEFAULT_TYPE) -> str:
        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        return message_text

    def get_photo(self, context: ContextTypes.DEFAULT_TYPE):
        return None

    def __editor_message_text(self, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        selected_field = self._get_selected_field(context)
        message_text = ""
        cursive_start_tag = "<i>" if self.parse_mode == ParseMode.HTML else "_"
        cursive_end_tag = "</i>" if self.parse_mode == ParseMode.HTML else "_"

        if selected_field.value is None or (isinstance(selected_field.value, list) and not selected_field.value):
            message_text += cursive_start_tag
            message_text += escape_markdown(_("Empty...")
                                            ) if self.parse_mode is ParseMode.MARKDOWN_V2 else _("Empty...")
            message_text += cursive_end_tag
        else:
            if selected_field.message_type is MessageType.PHOTO:
                if selected_field.allow_multiple_photos:
                    message_text += "\n".join([_("Photo") + f" {idx + 1}" for idx in range(len(selected_field.value))])
                else:
                    message_text += _("Photo")
            else:
                message_text += selected_field.value
        message_text += "\n"
        return message_text

    def __fields_message_text(self, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        bold_start_tag = "<b>" if self.parse_mode == ParseMode.HTML else "*"
        bold_end_tag = "</b>" if self.parse_mode == ParseMode.HTML else "*"
        active_fields = self._active_fields(context)
        selected_field = self._get_selected_field(context)
        message_text = ""
        for field in active_fields:
            if field.required and context.user_data[self.menu_name]['alert_required_fields'] and (
                field.value is None or (isinstance(field.value, list) and len(field.value) == 0)
            ):
                message_text += "⚠ "

            arrow_text = ("↦" if field is selected_field else '○ ')
            message_text += escape_markdown(arrow_text) if self.parse_mode == ParseMode.MARKDOWN_V2 else arrow_text

            clean_value = field.clean_value(context.user.language_code)
            if isinstance(clean_value, str) and len(clean_value) > 15:
                short_value = f"{clean_value[:15]}..."
                short_value = escape_markdown(short_value) if self.parse_mode is ParseMode.MARKDOWN_V2 else short_value
            else:
                short_value = clean_value

            if field is selected_field and clean_value is None:
                message_text += f"{bold_start_tag}{field.title}: "
                message_text += escape_markdown(_("Waiting...")
                                                ) if self.parse_mode is ParseMode.MARKDOWN_V2 else _("Waiting...")
                message_text += f"{bold_end_tag}\n"
            elif field.allow_multiple_choices and not field.value:
                message_text += f"{field.title}: {_('Empty')}\n"
            elif field is selected_field and clean_value is not None:
                message_text += f"{bold_start_tag}{field.title}: {short_value}{field.units}{bold_end_tag}\n"
            else:
                field_value = _('Empty') if clean_value is None else str(short_value) + field.units
                message_text += f"{field.title}: {field_value}\n"
        return message_text

    def message_text(self, context: ContextTypes.DEFAULT_TYPE) -> str:
        _ = context.user.translator
        cursive_start_tag = "<i>" if self.parse_mode == ParseMode.HTML else "_"
        cursive_end_tag = "</i>" if self.parse_mode == ParseMode.HTML else "_"

        message_text = self.message_top_text(context)
        message_text += self.separator_text(context)
        active_fields = self._active_fields(context)
        has_not_filled_required_fields = False

        if context.user_data[self.menu_name]['editor_active']:
            message_text += self.__editor_message_text(context)
        else:
            message_text += self.__fields_message_text(context)
            for field in active_fields:
                if field.required and context.user_data[self.menu_name]['alert_required_fields'] and (
                    field.value is None or (isinstance(field.value, list) and len(field.value) == 0)
                ):
                    has_not_filled_required_fields = True
                    break

        message_text += self.separator_text(context)
        message_text += self.message_bottom_text(context)
        if context.user_data[self.menu_name]['alert_required_fields'] and has_not_filled_required_fields:
            message_text += "\n" + cursive_start_tag + escape_markdown(_("⚠ - Required fields")) + cursive_end_tag
        return message_text

    def message_markup(self, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        active_fields = self._active_fields(context)
        selected_field = self._get_selected_field(context)

        buttons = []
        if not context.user_data[self.menu_name]['editor_active']:
            buttons.extend(self.controls(context))
        else:
            if selected_field.message_type is MessageType.PHOTO and selected_field.allow_multiple_photos:
                delete_photos_buttons = []
                for idx, photo in enumerate(selected_field.value):
                    delete_photos_buttons.append(
                        InlineKeyboardButton(
                            "🗑" + _("Photo") + f" {idx + 1}", callback_data=f"delete_photo_{self.menu_name}_{idx}"
                        )
                    )
                buttons.extend(group_buttons(delete_photos_buttons, 4))

        if selected_field is not None and selected_field.choices:
            values_buttons = []
            current_choices_page = selected_field.choices[
                self.choices_per_page * context.user_data[self.menu_name]['choices_page']:self.choices_per_page *
                context.user_data[self.menu_name]['choices_page'] + self.choices_per_page]
            for idx, choice in enumerate(current_choices_page):
                if isinstance(choice, Enum):
                    if hasattr(choice, 'title'):
                        choice_name = choice.title(context.user.language_code)
                    else:
                        choice_name = choice.value
                else:
                    choice_name = str(choice)
                if selected_field.allow_multiple_choices and choice in selected_field.value:
                    values_buttons.append(
                        InlineKeyboardButton(
                            "✅ " + choice_name,
                            callback_data=f"choice_{self.menu_name}_"
                            f"{context.user_data[self.menu_name]['choices_page'] * self.choices_per_page + idx}"
                        )
                    )
                else:
                    values_buttons.append(
                        InlineKeyboardButton(
                            choice_name,
                            callback_data=f"choice_{self.menu_name}_"
                            f"{context.user_data[self.menu_name]['choices_page'] * self.choices_per_page + idx}"
                        )
                    )

            buttons.extend(self.choices_controls(context, selected_field.choices))
            buttons.extend(group_buttons(values_buttons, selected_field.choices_group_size))

        if self.show_field_selectors and not context.user_data[self.menu_name]['editor_active']:
            field_buttons = []
            for field in active_fields:
                if not field.switchable:
                    continue

                depend_field = self._get_field(context, field.depend_field_name)
                if depend_field is None or (
                    depend_field and (
                        depend_field.value != field.depend_field_value
                        if field.depend_field_hides_current_field else depend_field.value == field.depend_field_value
                    )
                ):
                    button_text = re.sub('<[^<]+?>', '', field.name.replace("\\", ""))
                    field_buttons.append(
                        InlineKeyboardButton(button_text, callback_data=f"field_{self.menu_name}_{field.name}")
                    )

            buttons.extend(group_buttons(field_buttons, self.field_buttons_group_size))

        bottom_buttons = self.bottom_buttons(context)
        if bottom_buttons:
            buttons.append(bottom_buttons)

        if not context.user_data[self.menu_name]['editor_active']:
            save_button = self.save_button(context)
            if save_button:
                buttons.append([save_button])

            back_button = self.back_button(context)
            if back_button:
                buttons.append([back_button])
        else:
            back_button = self.back_from_editor_button(context)
            if back_button:
                buttons.append([back_button])

            cancel_editor_button = self.cancel_editor_button(context)
            if cancel_editor_button:
                buttons.append([cancel_editor_button])

        markup = InlineKeyboardMarkup(buttons)
        return markup

    async def send_message(self, context: ContextTypes.DEFAULT_TYPE):
        message_text = self.message_text(context)
        markup = self.message_markup(context)
        selected_field = self._get_selected_field(context)
        reply_to_message_id = None

        if context.user_data[self.menu_name]['editor_active'] \
                and selected_field.message_type is MessageType.PHOTO \
                and selected_field.value:
            if isinstance(selected_field.value, PhotoSize):
                photo = selected_field.value.file_id
            else:
                media = []
                for photo in selected_field.value:
                    media.append(InputMediaPhoto(photo.file_id))
                messages = await context.bot.send_media_group(chat_id=context.user.id, media=media)
                context.user_data[self.menu_name]['media_group_ids'] = [mes.message_id for mes in messages]
                reply_to_message_id = messages[0].message_id
                photo = None
        else:
            photo = self.get_photo(context)

        if photo is None:
            return await send_or_edit(
                context,
                chat_id=context.user.id,
                text=message_text,
                reply_markup=markup,
                parse_mode=self.parse_mode,
                disable_web_page_preview=self.disable_web_page_preview,
                reply_to_message_id=reply_to_message_id
            )
        else:
            return await send_or_edit(
                context,
                chat_id=context.user.id,
                photo=photo,
                caption=message_text,
                reply_markup=markup,
                parse_mode=self.parse_mode,
                disable_web_page_preview=self.disable_web_page_preview
            )

    async def change_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        context.user_data[self.menu_name]['alert_required_fields'] = False
        new_field_name = update.callback_query.data.replace(f"field_{self.menu_name}_", "")

        if new_field_name != context.user_data[self.menu_name]['selected_field_name']:
            context.user_data[self.menu_name]['selected_field_name'] = new_field_name
            selected_field = self._get_selected_field(context)
            if selected_field and selected_field.callback:
                return selected_field.callback(context)
            await self.send_message(context)

        await self.bot.answer_callback_query(update.callback_query.id)
        return self.States.DEFAULT

    async def previous_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)
        context.user_data[self.menu_name]['alert_required_fields'] = False
        field_names = cycle(reversed(context.user_data[self.menu_name]['fields'].keys()))

        new_field_name = None
        get_field = False
        for field_name in field_names:
            if get_field and self._get_field(context, field_name).switchable:
                new_field_name = field_name
                break

            if get_field and field_name == context.user_data[self.menu_name]['selected_field_name']:
                break

            if field_name == context.user_data[self.menu_name]['selected_field_name']:
                get_field = True

        context.user_data[self.menu_name]['selected_field_name'] = new_field_name
        active_field = self._get_field(context, new_field_name)

        if active_field and active_field.callback:
            return active_field.callback(context)

        if update.callback_query:
            await self.bot.answer_callback_query(update.callback_query.id)

        await self.send_message(context)
        return self.States.DEFAULT

    async def next_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)

        context.user_data[self.menu_name]['alert_required_fields'] = False
        field_names = cycle(context.user_data[self.menu_name]['fields'].keys())

        new_field_name = None
        get_field = False
        for field_name in field_names:
            if get_field and self._get_field(context, field_name).switchable:
                new_field_name = field_name
                break

            if get_field and field_name == context.user_data[self.menu_name]['selected_field_name']:
                break

            if field_name == context.user_data[self.menu_name]['selected_field_name']:
                get_field = True

        context.user_data[self.menu_name]['selected_field_name'] = new_field_name
        active_field = self._get_field(context, new_field_name)

        if active_field and active_field.callback:
            return active_field.callback(context)

        if update.callback_query:
            await self.bot.answer_callback_query(update.callback_query.id)

        await self.send_message(context)
        return self.States.DEFAULT

    async def previous_choices_page(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)
        selected_field = self._get_selected_field(context)
        choices_page = context.user_data[self.menu_name]['choices_page']
        context.user_data[self.menu_name]['alert_required_fields'] = False

        if choices_page <= 0:
            context.user_data[self.menu_name
                              ]['choices_page'] = ceil(len(selected_field.choices) / self.choices_per_page) - 1
        else:
            context.user_data[self.menu_name]['choices_page'] -= 1

        if update.callback_query:
            await self.bot.answer_callback_query(update.callback_query.id)

        await self.send_message(context)

        return self.States.DEFAULT

    async def next_choices_page(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)
        selected_field = self._get_selected_field(context)
        choices_page = context.user_data[self.menu_name]['choices_page']
        context.user_data[self.menu_name]['alert_required_fields'] = False

        if choices_page >= ceil(len(selected_field.choices) / self.choices_per_page) - 1:
            context.user_data[self.menu_name]['choices_page'] = 0
        else:
            context.user_data[self.menu_name]['choices_page'] += 1

        if update.callback_query:
            await self.bot.answer_callback_query(update.callback_query.id)

        await self.send_message(context)
        return self.States.DEFAULT

    async def __process_choice(self, context: ContextTypes.DEFAULT_TYPE, value: str):
        selected_field = self._get_selected_field(context)

        if not selected_field.value:
            selected_field.value = []

        value = selected_field.validate(value)
        if value in selected_field.value:
            selected_field.value.remove(value)
        else:
            selected_field.value.append(value)

        await self.send_message(context)
        return self.States.DEFAULT

    async def fill_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        selected_field = self._get_selected_field(context)
        context.user_data[self.menu_name]['alert_required_fields'] = False

        if self.remove_user_messages:
            await delete_user_message(update)

        if selected_field is None:
            await self.send_message(context)
            return self.States.DEFAULT

        if selected_field.choices and update.callback_query is None:
            await self.send_message(context)
            return self.States.DEFAULT

        if selected_field.message_type is MessageType.DOCUMENT:
            message = await self.bot.send_message(chat_id=context.user.id, text=_("Please send me document"))
            context.user_data[self.menu_name]["error_message_id"] = message.message_id
            await self.send_message(context)
            return self.States.DEFAULT
        elif selected_field.message_type is MessageType.PHOTO:
            message = await self.bot.send_message(chat_id=context.user.id, text=_("Please send me photo"))
            context.user_data[self.menu_name]["error_message_id"] = message.message_id
            await self.send_message(context)
            return self.States.DEFAULT
        elif selected_field.message_type is MessageType.VIDEO:
            message = await self.bot.send_message(chat_id=context.user.id, text=_("Please send me video"))
            context.user_data[self.menu_name]["error_message_id"] = message.message_id
            await self.send_message(context)
            return self.States.DEFAULT
        elif selected_field.message_type is MessageType.ANIMATION:
            message = await self.bot.send_message(chat_id=context.user.id, text=_("Please send me animation"))
            context.user_data[self.menu_name]["error_message_id"] = message.message_id
            await self.send_message(context)
            return self.States.DEFAULT
        elif selected_field.message_type is MessageType.STICKER:
            message = await self.bot.send_message(chat_id=context.user.id, text=_("Please send me sticker"))
            context.user_data[self.menu_name]["error_message_id"] = message.message_id
            await self.send_message(context)
            return self.States.DEFAULT
        elif selected_field.message_type is MessageType.LOCATION:
            return self.States.DEFAULT

        # obtain text
        if update.callback_query:
            choice_index = int(update.callback_query.data.replace(f"choice_{self.menu_name}_", ""))
            text = selected_field.choices[choice_index]
            await update.callback_query.answer()
            context.user_data[self.menu_name]['choices_page'] = 0
        else:
            if selected_field.parse_mode == ParseMode.MARKDOWN:
                text = update.message.text_markdown_urled
            elif selected_field.parse_mode == ParseMode.MARKDOWN_V2:
                text = update.message.text_markdown_v2_urled
            elif selected_field.parse_mode == ParseMode.HTML:
                text = update.message.text_html
            else:
                text = update.message.text

        # write text to user_data

        if selected_field.allow_multiple_choices and selected_field.choices:
            return await self.__process_choice(context, text)
        else:
            await self._delete_error_message(context)
            value, validator = selected_field.validate(text)
            if isinstance(value, ValueError):
                error_messages = validator.error_messages(context.user.language_code)
                message_text = list(error_messages)[-1]

                message = await context.bot.send_message(chat_id=context.user.id, text=message_text)
                context.user_data[self.menu_name]["error_message_id"] = message.message_id

                await self.send_message(context)
                return self.States.DEFAULT
            else:
                selected_field.value = value

        # change selected field
        if context.user_data[self.menu_name]['editor_active']:
            await self.send_message(context)
            return self.States.EDITOR
        else:
            return await self.next_field(update, context)

    async def _delete_error_message(self, context: ContextTypes.DEFAULT_TYPE):
        error_message_id = context.user_data[self.menu_name].get("error_message_id")
        if error_message_id:
            try:
                await context.bot.delete_message(context.user.id, error_message_id)
            except BadRequest:
                pass

    async def fill_media_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        selected_field = self._get_selected_field(context)
        context.user_data[self.menu_name]['alert_required_fields'] = False

        if selected_field is None:
            return self.States.DEFAULT

        # obtain text
        if update.message.photo and selected_field.message_type is MessageType.PHOTO:
            if selected_field.allow_multiple_photos:
                if len(selected_field.value) >= selected_field.max_photos_count:
                    await delete_user_message(update)
                    await self.bot.send_message(
                        chat_id=context.user.id,
                        text=_("You can't upload more than {limit} photos.").format(
                            limit=selected_field.max_photos_count
                        )
                    )
                    return self.States.EDITOR if context.user_data[self.menu_name
                                                                   ]['editor_active'] else self.States.DEFAULT

                selected_field.value.append(update.message.photo[-1])
            else:
                selected_field.value = update.message.photo[-1]
        elif update.message.document and selected_field.message_type is MessageType.DOCUMENT:
            selected_field.value = update.message.document.file_id
        elif update.message.video and selected_field.message_type is MessageType.VIDEO:
            selected_field.value = update.message.video.file_id
        elif update.message.animation and selected_field.message_type is MessageType.ANIMATION:
            selected_field.value = update.message.animation.file_id
        elif update.message.sticker and selected_field.message_type is MessageType.STICKER:
            selected_field.value = update.message.sticker.file_id
        else:
            await self.bot.send_message(
                chat_id=context.user.id, text=_("This field does not allow upload this type of content.")
            )
            await self.send_message(context)
            return self.States.DEFAULT

        await delete_user_message(update)
        if selected_field.message_type is MessageType.PHOTO and selected_field.allow_multiple_photos:
            await self.__delete_media_group(context)
            await self.send_message(context)
            return self.States.EDITOR if context.user_data[self.menu_name]['editor_active'] else self.States.DEFAULT
        else:
            return await self.next_field(update, context)

    async def clear_field(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        context.user_data[self.menu_name]['alert_required_fields'] = False
        await update.callback_query.answer()

        selected_field = self._get_selected_field(context)
        if selected_field is None:
            return self.States.DEFAULT

        selected_field.value = selected_field.default
        await self.send_message(context)
        return self.States.DEFAULT

    async def field_editor(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)

        _ = context.user.translator
        selected_field = self._get_selected_field(context)
        if selected_field.choices or selected_field.message_type not in [
            MessageType.TEXT, MessageType.PHOTO
        ] or not selected_field.allow_editor:
            await update.callback_query.answer(text=_("There is nothing to edit"))
            return self.States.DEFAULT

        context.user_data[self.menu_name]['editor_backup_value'] = copy(selected_field.value)
        context.user_data[self.menu_name]['editor_active'] = True
        context.user_data[self.menu_name]['alert_required_fields'] = False
        await self.send_message(context)
        return self.States.EDITOR

    async def __delete_media_group(self, context: ContextTypes.DEFAULT_TYPE, force_delete_interface: bool = True):
        if context.user_data[self.menu_name]['media_group_ids']:
            for message_in in context.user_data[self.menu_name]['media_group_ids']:
                await self.bot.delete_message(chat_id=context.user.id, message_id=message_in)
            context.user_data[self.menu_name]['media_group_ids'] = []
            if force_delete_interface:
                await delete_interface(context)

    async def exit_editor(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)

        context.user_data[self.menu_name]['editor_active'] = False
        context.user_data[self.menu_name]['alert_required_fields'] = False

        await self.__delete_media_group(context)

        await self.send_message(context)
        return self.States.DEFAULT

    async def cancel_editor(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        await self._delete_error_message(context)
        selected_field = self._get_selected_field(context)
        selected_field.value = context.user_data[self.menu_name]['editor_backup_value']
        context.user_data[self.menu_name]['alert_required_fields'] = False

        context.user_data[self.menu_name]['editor_active'] = False
        await self.__delete_media_group(context)

        await self.send_message(context)
        return self.States.DEFAULT

    async def delete_photo(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator
        selected_field = self._get_selected_field(context)
        if not selected_field.allow_multiple_photos:
            await update.callback_query.answer(text=_("You can't delete this photo"))
            return self.States.EDITOR

        index = int(update.callback_query.data.replace(f'delete_photo_{self.menu_name}_', ''))
        selected_field.value.pop(index)
        await self.__delete_media_group(context)
        await self.send_message(context)
        return self.States.EDITOR

    def _check_required_fields(self, context: ContextTypes.DEFAULT_TYPE):
        fields = context.user_data[self.menu_name]['fields']
        for field_name, field in fields.items():
            depend_field = self._get_field(context, field.depend_field_name)
            if depend_field is None or (
                depend_field and (
                    depend_field.value != field.depend_value
                    if field.depend_field_hides_current_field else depend_field.value == field.depend_value
                )
            ):
                if field.required and field.value is None or (isinstance(field.value, list) and len(field.value) == 0):
                    return False

        return True

    async def _save(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        _ = context.user.translator

        if not self._check_required_fields(context):
            await update.callback_query.answer(text=_("Please fill required fields."), show_alert=True)
            context.user_data[self.menu_name]['alert_required_fields'] = True
            await self.send_message(context)
            return self.States.DEFAULT

        return await self.save(update, context)

    def save(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        raise NotImplementedError

    def get_handler(self):
        states = {
            self.States.DEFAULT: [
                CallbackQueryHandler(self.next_field, pattern=f'^next_field_{self.menu_name}$'),
                CallbackQueryHandler(self.previous_field, pattern=f'^previous_field_{self.menu_name}$'),
                CallbackQueryHandler(self.next_choices_page, pattern=f'^next_choice_{self.menu_name}$'),
                CallbackQueryHandler(self.previous_choices_page, pattern=f'^previous_choice_{self.menu_name}$'),
                CallbackQueryHandler(self._save, pattern=f'^save_{self.menu_name}$'),
                CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                CallbackQueryHandler(self.change_field, pattern=fr'^field_{self.menu_name}_.+$'),
                CallbackQueryHandler(self.fill_field, pattern=fr'^choice_{self.menu_name}_\d+$'),
                CallbackQueryHandler(self.clear_field, pattern=f'^clear_field_{self.menu_name}$'),
                CallbackQueryHandler(self.field_editor, pattern=f"^edit_field_{self.menu_name}$"),
                MessageHandler(filters.TEXT, self.fill_field),
                MessageHandler(
                    filters.PHOTO | filters.Document.ALL | filters.ANIMATION | filters.VIDEO | filters.Sticker.ALL,
                    self.fill_media_field
                ),
                # MessageHandler(filters.LOCATION, self.fill_location_field)
            ],
            self.States.EDITOR: [
                CallbackQueryHandler(self.exit_editor, pattern=f"^exit_editor_{self.menu_name}$"),
                CallbackQueryHandler(self.cancel_editor, pattern=fr"^cancel_editor_{self.menu_name}$"),
                CallbackQueryHandler(self.delete_photo, pattern=fr"^delete_photo_{self.menu_name}_\d+$"),
                MessageHandler(filters.TEXT, self.fill_field),
                MessageHandler(filters.PHOTO, self.fill_media_field),
            ]
        }

        additional_states = self.states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(
            entry_points=self.entry_points(),
            states=states,
            fallbacks=self.fallbacks(),
            allow_reentry=True,
            name=self.menu_name,
            persistent=True
        )

        return handler
