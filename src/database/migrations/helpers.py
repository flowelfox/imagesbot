from src.scripts.alembic import op
import sqlalchemy as sa


def upgrade_enum(name: str, old_values: [str], new_values: [str], on_tables: [(str, str)], forced_default=None):
    """Replaces an enum's list of values.

    Args:
        name: Name of the enum
        new_values: New list of values
        old_values: Old list of values
        on_tables: List of tuples of table name
        and column to modify (which actively use the enum).
        Assumes each column has a default val.
    """
    connection = op.get_bind()

    tmp_name = "{}_tmp".format(name)

    # Rename old type
    op.execute("ALTER TYPE {} RENAME TO {};".format(name, tmp_name))

    # Create new type
    lsl = sa.Enum(*new_values, name=name)
    lsl.create(connection)

    # Replace all usages
    for (table, column) in on_tables:
        # Get default to re-set later
        default_typed = connection.execute(
            "SELECT column_default FROM information_schema.columns WHERE table_name='{table}' "
            "AND column_name='{column}';".format(table=table, column=column)
        ).first()[0]

        # Is bracketed already

        if forced_default:
            default = f"'{forced_default}'"
        elif default_typed:
            default = default_typed[:default_typed.index("::")]
        else:
            default = None

        if default is not None:
            # Set all now invalid values to default
            update_query = """
                UPDATE {table}
                SET {column}={default}
                WHERE {column} NOT IN {allowed};
                """
            update_query_single_filter = """
                UPDATE {table}
                SET {column}={default}
                WHERE {column} != '{allowed}';
                """
            allowed = tuple(set(old_values).intersection(set(new_values)))
            if len(allowed) > 1:
                connection.execute(
                    update_query.format(
                        table=table,
                        column=column,
                        # Invalid: What isn't contained in both new and old
                        # Can't just remove what's not in new because we get
                        # a type error
                        allowed=allowed,
                        default=default
                    )
                )
            else:
                connection.execute(
                    update_query_single_filter.format(table=table, column=column, allowed=allowed[0], default=default)
                )

        alter_table = "ALTER TABLE {table}\n".format(table=table)
        if default is not None:
            alter_query = alter_table + "ALTER COLUMN {column} DROP DEFAULT;"
            op.execute(alter_query.format(column=column))

        alter_query = alter_table + "ALTER COLUMN {column} TYPE {enum_name} USING {column}::text::{enum_name};"
        op.execute(alter_query.format(column=column, enum_name=name))

        if default is not None:
            alter_query = alter_table + "ALTER COLUMN {column} SET DEFAULT {default};"
            op.execute(alter_query.format(column=column, default=default))

    # Remove old type
    op.execute("DROP TYPE {};".format(tmp_name))
