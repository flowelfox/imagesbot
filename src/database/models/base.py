import logging

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import DeclarativeBase, object_session

logger = logging.getLogger(__name__)


class Base(DeclarativeBase):
    def save(self):
        """Save object into database table."""
        session = object_session(self)

        with session.begin():
            try:
                session.add(self)
                return self
            except SQLAlchemyError as e:
                session.rollback()
                logger.critical(f"Database error while saving {self} object: {str(e)}")
                return None

    def delete(self):
        """Remove object from database table."""
        session = object_session(self)

        with session.begin():
            try:
                session.delete(self)
                return self
            except SQLAlchemyError as e:
                session.rollback()
                logger.critical(f"Database error while removing {self.__class__} object: {str(e)}")
                return None
