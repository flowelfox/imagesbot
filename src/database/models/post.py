import datetime
from typing import TYPE_CHECKING

from sqlalchemy import Integer, DateTime, ForeignKey, func
from sqlalchemy.orm import Mapped, mapped_column, relationship, object_session

from src.database.enums import ReactionType
from .base import Base
from .reaction import Reaction

if TYPE_CHECKING:
    from src.database.models.image import Image

class Post(Base):
    __tablename__ = 'posts'

    # Columns
    id: Mapped[int] = mapped_column(Integer, primary_key=True)

    image_id: Mapped[int] = mapped_column(Integer, ForeignKey('images.id'), unique=True)
    image: Mapped['Image'] = relationship("Image", back_populates="post")

    create_date: Mapped[datetime.datetime] = mapped_column(DateTime, default=datetime.datetime.utcnow)

    def likes(self):
        session = object_session(self)
        return session.query(func.count(Reaction.id)).filter(Reaction.post_id == self.id).filter(Reaction.type == ReactionType.like).scalar()

    def dislikes(self):
        session = object_session(self)
        return session.query(func.count(Reaction.id)).filter(Reaction.post_id == self.id).filter(Reaction.type == ReactionType.dislike).scalar()
