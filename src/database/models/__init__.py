import logging

from sqlalchemy import Pool
from sqlalchemy.event import listens_for

from .base import Base
from .image import Image
from .post import Post
from .reaction import Reaction
from .user import User

logger = logging.getLogger(__name__)


@listens_for(Pool, "connect")
def on_connect(conn, connection_record):
    if conn.info.dsn_parameters.get("krbsrvname") == "postgres":
        with conn.cursor() as curs:
            curs.execute("SET idle_in_transaction_session_timeout TO 0;")
            curs.execute("SET SESSION idle_in_transaction_session_timeout TO 0;")
            curs.execute("COMMIT;")

    logger.info(f"New session created: {conn.info.dsn_parameters.get('application_name', 'UNKNOWN')}")


@listens_for(Pool, "close")
def on_close(conn, connection_record):
    logger.info(f"Session closed: {conn.info.dsn_parameters.get('application_name', 'UNKNOWN')}")


__all__ = ["Base", "User", "Post", "Image", "Reaction"]
