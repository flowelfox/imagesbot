import datetime
import gettext
from html import escape
from typing import Optional, TYPE_CHECKING

from sqlalchemy import BigInteger, String, DateTime, ForeignKey, Table, Column
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column, relationship
from telegram import Bot, User as TelegramUser

from src.database.models.base import Base
from src.settings import PROJECT_ROOT


class User(Base):
    __tablename__ = 'users'

    # Columns
    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    first_name: Mapped[str] = mapped_column(String)
    last_name: Mapped[Optional[str]] = mapped_column(String)
    username: Mapped[Optional[str]] = mapped_column(String)
    language_code: Mapped[str] = mapped_column(String)
    came_from: Mapped[Optional[str]] = mapped_column(String)
    join_date: Mapped[datetime.datetime] = mapped_column(DateTime, default=datetime.datetime.utcnow)
    block_date: Mapped[Optional[datetime.datetime]] = mapped_column(DateTime)


    @hybrid_property
    def is_active(self):
        return not self.block_date

    # noinspection PyMethodParameters
    @is_active.expression
    def is_active(cls):
        return cls.block_date.is_(None)

    def activate(self):
        self.block_date = None

    def deactivate(self):
        self.block_date = datetime.datetime.utcnow()

    @property
    def name(self) -> str:
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"
        elif self.first_name and not self.last_name:
            return f"{self.first_name}"
        else:
            return ""

    @property
    def mention_url(self) -> str:
        return f"tg://user?id={self.id}"

    @property
    def mention(self) -> str:
        return f"<a href=\"{self.mention_url}\">{escape(self.name)}</a>"

    def to_telegram_user(self, bot: Bot, **kwargs) -> TelegramUser:
        return TelegramUser(
            self.id, self.first_name, False, self.last_name, self.username, self.language_code, bot=bot, **kwargs
        )

    @property
    def _translation(self):
        if self.language_code is None:
            translation = gettext.translation(
                'messages', str(PROJECT_ROOT / 'locales'), languages=['en'], fallback=True
            )
        else:
            translation = gettext.translation(
                'messages', str(PROJECT_ROOT / 'locales'), languages=[str(self.language_code)], fallback=True
            )
        return translation

    @property
    def translator(self):
        return self._translation.gettext

    @property
    def ntranslator(self):
        return self._translation.ngettext