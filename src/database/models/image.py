import datetime
from typing import TYPE_CHECKING

from sqlalchemy import Integer, String, DateTime
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import Base
if TYPE_CHECKING:
    from src.database.models.post import Post


class Image(Base):
    __tablename__ = 'images'

    # Columns
    id: Mapped[int] = mapped_column(Integer, primary_key=True)

    file_id: Mapped[str] = mapped_column(String, unique=True)
    file_unique_id: Mapped[str] = mapped_column(String, unique=True)
    hash: Mapped[str] = mapped_column(String, unique=True)
    post: Mapped['Post'] = relationship("Post", back_populates="image")
    create_date: Mapped[datetime.datetime] = mapped_column(DateTime, default=datetime.datetime.utcnow)