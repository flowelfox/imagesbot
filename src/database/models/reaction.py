from typing import TYPE_CHECKING

from sqlalchemy import Integer, ForeignKey, Enum
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.database.enums import ReactionType
from src.database.models import Base

if TYPE_CHECKING:
    from src.database.models.post import Post


class Reaction(Base):
    __tablename__ = 'reactions'

    # Columns
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    type: Mapped[ReactionType] = mapped_column(Enum(ReactionType))
    user_chat_id: Mapped[int] = mapped_column(Integer)

    post_id: Mapped[int] = mapped_column(Integer, ForeignKey('posts.id'))
    post: Mapped['Post'] = relationship("Post")