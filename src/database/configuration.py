from sqlalchemy import create_engine

from src.settings import DATABASE_URL, POOL_SIZE, MAX_OVERFLOW
from sqlalchemy.orm import sessionmaker, Session

connect_args = {
    'application_name': "MarketplaceBot"
}
engine = create_engine(DATABASE_URL, pool_size=POOL_SIZE, max_overflow=MAX_OVERFLOW, connect_args=connect_args)
sm = sessionmaker(bind=engine, autoflush=False, autocommit=False)


def get_session() -> Session:
    return sm()
