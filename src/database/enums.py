from enum import Enum

class ReactionType(Enum):
    like = "like"
    dislike = "dislike"
